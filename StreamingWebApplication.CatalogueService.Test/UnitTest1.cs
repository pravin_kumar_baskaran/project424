﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.CatalogueService.Raven;

namespace StreamingWebApplication.CatalogueService.Test
{
	[TestClass]
	public class UnitTest1
	{
		private const string Data = "../../data/youtube";

		[TestMethod]
		public void TestMethod1()
		{
			ICatalogueService catalogueService = new Raven.RavenCatalogueService(RavenDbHelper.GetDocumentStore());

			Assert.AreEqual(catalogueService.GetAllCarouselCollection().Carousels.Any(),true);
		}

		[TestMethod]
		public void TestMethod2()
		{
			ICatalogueService catalogueService = new Raven.RavenCatalogueService(RavenDbHelper.GetDocumentStore());
			Assert.IsNotNull(catalogueService);
		}
	}
}
