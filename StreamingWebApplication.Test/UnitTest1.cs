﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StreamingWebApplication.CatalogueService;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.Controllers;
using Moq;
using StreamingWebApplication.Models.ContentCollection;

namespace StreamingWebApplication.Test
{
	[TestClass]
	public class UnitTest1
	{

		private static ICatalogueService _catalogueService;
		private static Mock _mockCatalogueService;

		[ClassInitialize]
		public static void ClassSetup(TestContext a)
		{
			var _mockCatalogueService = new Moq.Mock<ICatalogueService>();
			_mockCatalogueService.Setup(catalogueService => catalogueService.GetAllCarouselCollection())
				.Returns(
					() =>
						new CarouselCollection()
						{
							Carousels =
								new List<Carousel>()
								{
									new Carousel() {CarouselItems = new List<ContentViewModel>() {new ContentViewModel() {ContentId = ""}}}
								}
						});


			_catalogueService = _mockCatalogueService.Object;
		}

		[TestMethod]
		public void TestMethod1()
		{
			HomeController homeController = new HomeController();

			var result = homeController.Index() as ViewResult;

			
			var carouselCollection = result.Model as CarouselCollection;

			Assert.AreEqual(carouselCollection.Carousels.Count>0,true);
		}
	}
}
