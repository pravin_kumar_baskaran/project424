﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using StreamingWebApplication.Models;

namespace CmsDownloader
{
	public class YoutubeHelper
	{
		public async Task<IList<PlaylistItem>> GetYoutubePlaylistItemsFromPlaylist(string playlistId)
		{
			var playlistItems = new List<PlaylistItem>();

			UserCredential credential;
			using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read))
			{
				credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
					GoogleClientSecrets.Load(stream).Secrets,
					// This OAuth 2.0 access scope allows for full read/write access to the
					// authenticated user's account.
					new[] {YouTubeService.Scope.Youtube},
					"user",
					CancellationToken.None,
					new FileDataStore(this.GetType().ToString())
					);
			}

			var youtubeService = new YouTubeService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = this.GetType().ToString()
			});

			var request = youtubeService.PlaylistItems.List("id,snippet,contentDetails");
			request.PlaylistId = playlistId;
			request.MaxResults = 50;
			var resposne = request.Execute();
			playlistItems.AddRange(resposne.Items);

			while (!string.IsNullOrWhiteSpace(resposne.NextPageToken))
			{
				request.PageToken = resposne.NextPageToken;
				resposne = request.Execute();
				playlistItems.AddRange(resposne.Items);
			}

			return playlistItems;
		}

		/// <summary>
		/// Get youtube video object for the given videoid
		/// </summary>
		/// <param name="videoId"></param>
		/// <returns></returns>
		public async Task<Video> GetYoutubeVideoDetails(string videoId)
		{
			Video returnVideo = null;

			UserCredential credential;
			using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read))
			{
				credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
					GoogleClientSecrets.Load(stream).Secrets,
					// This OAuth 2.0 access scope allows for full read/write access to the
					// authenticated user's account.
					new[] {YouTubeService.Scope.Youtube},
					"user",
					CancellationToken.None,
					new FileDataStore(this.GetType().ToString())
					);
			}

			var youtubeService = new YouTubeService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = this.GetType().ToString()
			});

			var request = youtubeService.Videos.List("id,snippet,contentDetails,statistics");
			request.Id = videoId;
			var resposne = request.Execute();
			returnVideo = resposne.Items.FirstOrDefault();


			return returnVideo;
		}


		public async Task<Video> UploadYoutubeVideoToKondatamPlaylist(ContentDetail contentDetail, string fileToUpload)
		{
			UserCredential credential;
			using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read))
			{
				credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
					GoogleClientSecrets.Load(stream).Secrets,
					// This OAuth 2.0 access scope allows an application to upload files to the
					// authenticated user's YouTube channel, but doesn't allow other types of access.
					new[] { YouTubeService.Scope.YoutubeUpload },
					"user",
					CancellationToken.None
				);
			}

			var youtubeService = new YouTubeService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
			});

			var video = new Video();
			video.Snippet = new VideoSnippet();
			video.Snippet.Title = contentDetail.Title;
			video.Snippet.Description = "www.kondatam.com";
			video.Snippet.Tags = new string[] {"shortfilm","tamil"  };
			video.Snippet.CategoryId = "1"; // See https://developers.google.com/youtube/v3/docs/videoCategories/list
			video.Status = new VideoStatus();
			video.Status.PrivacyStatus = "public"; // or "private" or "public"
			var filePath = fileToUpload; // Replace with path to actual movie file.

			using (var fileStream = new FileStream(filePath, FileMode.Open))
			{
				const int KB = 0x400;
				var minimumChunkSize = 256 * KB;


				var videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", fileStream, "video/*");
				videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
				videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;
				videosInsertRequest.ChunkSize = minimumChunkSize*4;
				var a =  videosInsertRequest.Upload();
				
			}
			
			File.Move(fileToUpload,"uploaded/"+fileToUpload);
			return video;
		}

		private void videosInsertRequest_ProgressChanged(IUploadProgress progress)
		{
			switch (progress.Status)
			{
				case UploadStatus.Uploading:
					Console.WriteLine("{0} bytes sent.", progress.BytesSent);
					break;

				case UploadStatus.Failed:
					Console.WriteLine("An error prevented the upload from completing.\n{0}", progress.Exception);
					break;
			}
		}

		private void videosInsertRequest_ResponseReceived(Video video)
		{
			Console.WriteLine("Video id '{0}' was successfully uploaded.", video.Id);	
		}
	}
}
