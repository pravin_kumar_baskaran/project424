﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Xml;
using Google.Apis.YouTube.v3.Data;
using HtmlAgilityPack;
using Raven.Abstractions.Extensions;
using Raven.Client;
using StreamingWebApplication.CatalogueService.Raven;
using StreamingWebApplication.Models;

namespace CmsDownloader
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			LoadContents("");
		}

		private void LoadContents(string searchText)
		{
			using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				contentDetails = session.Query<ContentDetail>().Take(1000).ToList();			
			}

			if (!string.IsNullOrWhiteSpace(searchText))
				contentDetails = contentDetails.Where(cd => cd.Title.ToLower().Contains(searchText.ToLower())).ToList();

			dgContents.ItemsSource = contentDetails;
		}

		IList<PlaylistItem>  _playlistItems = new List<PlaylistItem>();

		/// <summary>
		/// Downloads playlistitems and store in internal memory
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnDownloadDataPlaylist_Click(object sender, RoutedEventArgs e)
		{
			YoutubeHelper youtubeHelper = new YoutubeHelper();
			CancellationToken cancellationToken = new CancellationToken();

			
			var task = youtubeHelper.GetYoutubePlaylistItemsFromPlaylist(txtPlaylistId.Text);
			
			task.Wait(cancellationToken);

			txtPlaylisItemsCount.Content = task.Result.Count;
			_playlistItems = task.Result;
		}

		/// <summary>
		/// Saves PlaylistItemsInMemoryToDb
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSavePlaylistItems_Click(object sender, RoutedEventArgs e)
		{
			using (IDocumentSession session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				_playlistItems.ForEach(p =>
				{
					session.Store(p);
				});				

				session.SaveChanges();
			}
			_playlistItems.Clear();
			txtPlaylisItemsCount.Content = _playlistItems.Count;
		}

		private void btnDownloadPlaylitsItems_Click(object sender, RoutedEventArgs e)
		{

			List<ContentDetail> videosToDownload = new List<ContentDetail>();
			
			using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				videosToDownload = session.Query<ContentDetail>().Take(1000).ToList().Where(c=>c.SelfHostedLink==null).ToList();
			}

			new Thread(() =>
			{
				txtTotalVideosToDownload.Dispatcher.Invoke(() =>
				{
					txtTotalVideosToDownload.Content = videosToDownload.Count.ToString();
				});
			}).Start();


			int currentCount = 1;
			foreach (var video in videosToDownload)
			{
				new Thread(() =>
				{
					txtCurrentVideoDownloading.Dispatcher.Invoke(() =>
					{
						txtCurrentVideoDownloading.Content = currentCount.ToString();
						currentCount++;
					});
				}).Start();


				if (!File.Exists(video.Title))
				{
					var process = new Process
					{
						StartInfo = new ProcessStartInfo
						{
							FileName = @"ExternalLib\youtube-dl.exe",
							Arguments = video.Link
						}
					};
					process.Start();
					process.WaitForExit();
				}
			}

		}

		private void DeleteAllContentDetails()
		{

			using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				var contentDetails = session.Query<ContentDetail>().Take(1000);
				contentDetails.ForEach(p =>
				{
					session.Delete<ContentDetail>(p);
				});
				session.SaveChanges();
			}

		}

		private void btnConvertToContentDetails_Click(object sender, RoutedEventArgs e)
		{
			// commentd for secutiry DeleteAllContentDetails();

			using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				var playlistItems = session.Query<PlaylistItem>().Take(1000);
				var videos = session.Query<Video>().Take(1000).ToList();
				var exportsvideos = session.Query<Exports>().Take(1000).ToList();
				playlistItems.ForEach(p =>
				{
					var video = videos.FirstOrDefault(v => v.Id == p.ContentDetails.VideoId);
					var export = exportsvideos.FirstOrDefault(exp => exp.actuallink == p.ContentDetails.VideoId);
					var keywords = new List<string>();
					/*
					if (p.Snippet.PlaylistId.Equals("PLiJk2KIk0Obr_J2h2ykL7iTsfnv0N9gLQ"))
						keywords = new List<string>() {"Thriller", "Horror"};

					if (p.Snippet.PlaylistId.Equals("PLiJk2KIk0ObpbfFHMFxLLMuqu-RSjMvaH"))
						keywords = new List<string>() { "Comedy" };

					if (p.Snippet.PlaylistId.Equals("PLiJk2KIk0Obqr_x4S8hxObWbn7Zvkg4NQ"))
						keywords = new List<string>() { "Soft" };

					if (p.Snippet.PlaylistId.Equals("PLiJk2KIk0Obrq_8dSVAran-ntH5r77u0j"))
						keywords = new List<string>() { "Drama" };

					if (p.Snippet.PlaylistId.Equals("PLiJk2KIk0ObrdMaTf2airDOvPmoD-d9Tj"))
						keywords = new List<string>() { "Love" };

					if (p.Snippet.PlaylistId.Equals("PLiJk2KIk0ObrGDm1n4bmozkrqpynkGWU0"))
						keywords = new List<string>() { "Awareness" };
					*/


					var contentDetails = new ContentDetail()
					{
						Id = p.ContentDetails.VideoId,
						Actors = new List<string>() { "" },
						CreatedOn = DateTime.Now,
						Directors = new List<string>() { "" },
						Keywords = keywords,						
						Link = p.ContentDetails.VideoId,
						ModifiedOn = DateTime.Now,
						Synopsis = p.Snippet.Description,
						ThumbnailImagePath = new List<string>(),
						Title = p.Snippet.Title,
						ReleasedOn = p.Snippet.PublishedAt ?? DateTime.Now
					};

					if (video != null)
					{
						contentDetails.DurationIsoFormat = video.ContentDetails.Duration;
						contentDetails.ViewsCount = video.Statistics.ViewCount??0;
					}

					if (export != null)
					{
						contentDetails.Title = export.Title;

						if (export.Thriller == "1")
							contentDetails.Keywords.Add("Thriller");

						if (export.Romantic == "1")
							contentDetails.Keywords.Add("Romance");

						if (export.Suspense == "1")
							contentDetails.Keywords.Add("Suspense");

						if (export.Horror == "1")
							contentDetails.Keywords.Add("Horror");

						if (export.Drama == "1")
							contentDetails.Keywords.Add("Drama");

						if (export.Awareness == "1")
							contentDetails.Keywords.Add("Awareness");

						if (export.Comedy == "1")
							contentDetails.Keywords.Add("Comedy");

						if (export.Action == "1")
							contentDetails.Keywords.Add("Action");
					}

					if (p.Snippet.Thumbnails != null)
					{
						if (p.Snippet.Thumbnails.Medium != null)
							contentDetails.ThumbnailImagePath.Add(p.Snippet.Thumbnails.Medium.Url);

						if(p.Snippet.Thumbnails.Default != null)
							contentDetails.ThumbnailImagePath.Add(p.Snippet.Thumbnails.Default.Url);

					}

					try
					{
						session.Store(contentDetails,"ContentDetails/");

					}
					catch (Exception)
					{
					}
				});

				session.SaveChanges();
			}
		}

		/// <summary>
		/// Get videofrom the playlistitems in db and store video
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnGetVideoDetails_Click(object sender, RoutedEventArgs e)
		{
			YoutubeHelper youtubeHelper = new YoutubeHelper();
			CancellationToken cancellationToken = new CancellationToken();
			
			using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				var playlistItems = session.Query<PlaylistItem>().Take(1000);

				foreach (var playlistItem in playlistItems)
				{
					var task = youtubeHelper.GetYoutubeVideoDetails(playlistItem.ContentDetails.VideoId);
					task.Wait(cancellationToken);
					var video = task.Result;
					try
					{
						if (video != null)
							session.Store(video);

					}
					catch (Exception)
					{
					}
				}

				session.SaveChanges();
			}

		}

		private async void btnUploadToNewPlaylist_Click(object sender, RoutedEventArgs e)
		{
			YoutubeHelper youtubeHelper = new YoutubeHelper();
			List<ContentDetail> contentDetails = new List<ContentDetail>();
			using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				contentDetails = session.Query<ContentDetail>().Take(1000).ToList();


				foreach (var file in Directory.EnumerateFiles("."))
				{
					if (!file.Contains(".mp4"))
						continue;


					var linkId = file.Split('-').LastOrDefault().Split('.').FirstOrDefault();
					var contentDetail = contentDetails.FirstOrDefault(c => c.Link == linkId);
					if(contentDetail==null)
						continue;
					var newVideo = await youtubeHelper.UploadYoutubeVideoToKondatamPlaylist(contentDetail, file);

					contentDetail.SelfHostedLink = newVideo.Id;
					session.Store(contentDetails);
				}

				session.SaveChanges();
			}
		}

		private void btnUpdateSelfHostedLink(object sender, RoutedEventArgs e)
		{
			YoutubeHelper youtubeHelper = new YoutubeHelper();
			CancellationToken cancellationToken = new CancellationToken();
			var task = youtubeHelper.GetYoutubePlaylistItemsFromPlaylist("UUINoo4i2cOBKDFcBwpRVndA");
			
			task.Wait(cancellationToken);

			var playlistItems = task.Result;
	
			
			using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
			{
				var videos = session.Query<ContentDetail>().Take(1000).ToList();
				videos.ForEach(contentDetail =>
				{
					var playlistItem = playlistItems.Where(p => p.Snippet.Title == contentDetail.Title);

					if (playlistItem.Count() == 1)
					{

						contentDetail.SelfHostedLink = playlistItem.FirstOrDefault().ContentDetails.VideoId;						
					}
					else
					{
						var p = playlistItem;
					}

				});

				session.SaveChanges();
			}


		}

		private static int page = 1;
		private List<ContentDetail> contentDetails;

		//Check website Galata.com and download videosIds to a file
		private async void btnGetContentFromGalata_Click(object sender, RoutedEventArgs e)
		{
			HashSet<string> videoIds = new HashSet<string>();

			var htmlWeb = new HtmlWeb();

			for (; page <= 4; page++)
			{
				try
				{
					htmlWeb.PreRequest = new HtmlWeb.PreRequestHandler(OnPreRequest);
					var document = htmlWeb.Load("http://www.galatta.com/ajax_short_film.php", "POST");
					foreach (HtmlNode linkNode in document.DocumentNode.SelectNodes("//a[@href]"))
					{
						HtmlAttribute att = linkNode.Attributes["href"];
						var link = att.Value;

						if (link.Contains("short-film"))
						{
							var splits = link.Split('/');
							var id = splits[splits.Count() - 2];
							if (id != null && !videoIds.Contains(id))
							{

								videoIds.Add(id);
								//saveid
							}
						}
					}

				}
				catch (Exception)
				{
				}
				
			}


			using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"newVideoIds.txt",false))
			{
				foreach (string line in videoIds)
				{
					file.WriteLine(line);
				}
			}
		}

		public string AssemblePostPayload()
		{
			var k = new List<KeyValuePair<string, string>>()
			{
				new KeyValuePair<string, string>("page", ""+page),
				new KeyValuePair<string, string>("language", "1")
			};
			StringBuilder sb = new StringBuilder();
			foreach (var element in k)
			{
				string value = element.Value;
				sb.Append("&" + element.Key + "=" + value);
			}
			return sb.ToString().Substring(1);
		}
		private bool OnPreRequest(HttpWebRequest request)
		{
			string payload = AssemblePostPayload();
			byte[] buff = Encoding.UTF8.GetBytes(payload.ToCharArray());
			request.ContentLength = buff.Length;
			request.ContentType = "application/x-www-form-urlencoded";
			System.IO.Stream reqStream = request.GetRequestStream();
			reqStream.Write(buff, 0, buff.Length);
			return true;
		}

		private async void btnToContentDetails_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtVideoId.Text))
			{
				MessageBox.Show("Enter Video Id");
				return;
			}

			if (string.IsNullOrWhiteSpace(txtKeywords.Text))
			{
				MessageBox.Show("Enter keywords");
				return;
			}

			if (string.IsNullOrWhiteSpace(txtTitle.Text))
			{
				MessageBox.Show("Enter Title");
				return;
			}

			var videoId = txtVideoId.Text;
			var keywords = txtKeywords.Text.Split(',');
			var title = txtTitle.Text;

			// download videoInfo
			var youtubeHelper = new YoutubeHelper();
			var videoDetail = await  youtubeHelper.GetYoutubeVideoDetails(videoId);
			
			// check if the video exists by id or name
			
			if (contentDetails.Any(c => c.Link.Equals(videoId, StringComparison.InvariantCultureIgnoreCase)))
			{
				MessageBox.Show("Idexists");
			}
			if (contentDetails.Any(c => c.Title.Equals(videoDetail.Snippet.Title, StringComparison.InvariantCultureIgnoreCase)))
			{
				MessageBox.Show("name exists");
			}

			// create new  contentdetails
			var contentDetailInfo = new ContentDetail()
			{
				Id = videoId,
				Actors = new List<string>() { "" },
				CreatedOn = DateTime.Now,
				Directors = new List<string>() { "" },
				Keywords = keywords,
				Link = videoId,
				ModifiedOn = DateTime.Now,
				Synopsis = videoDetail.Snippet.Description,
				ThumbnailImagePath = new List<string>(),
				Title = title,
				ReleasedOn = videoDetail.Snippet.PublishedAt ?? DateTime.Now,
				DurationIsoFormat = videoDetail.ContentDetails.Duration,
				ViewsCount = videoDetail.Statistics.ViewCount ?? 0
			};
			

			if (videoDetail.Snippet.Thumbnails != null)
			{
				if (videoDetail.Snippet.Thumbnails.Medium != null)
					contentDetailInfo.ThumbnailImagePath.Add(videoDetail.Snippet.Thumbnails.Medium.Url);

				if (videoDetail.Snippet.Thumbnails.Default != null)
					contentDetailInfo.ThumbnailImagePath.Add(videoDetail.Snippet.Thumbnails.Default.Url);
			}

			try
			{
				using (var session = RavenDbHelper.GetDocumentStore().OpenSession())
				{
					session.Store(contentDetailInfo, "ContentDetails/");
					session.SaveChanges();
				}

			}
			catch (Exception)
			{
			}

			// add contentdetails
		}

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			LoadContents(tbFilterContents.Text);
		}
	}


	public class Exports
	{
		public string Id { get; set; }
		public string actuallink { get; set; }
		public string Title { get; set; }
		public string Thriller { get; set; }
		public string Romantic { get; set; }
		public string Suspense { get; set; }
		public string Horror { get; set; }
		public string Action { get; set; }
		public string Drama { get; set; }
		public string Comedy { get; set; }
		public string Awareness { get; set; }

	}
}
