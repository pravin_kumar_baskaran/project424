﻿using System.Web.Mvc;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.CatalogueService.Raven;

namespace StreamingWebApplication.Controllers
{
    public class SearchController : RavenController
    {
		private readonly ICatalogueService _catalogueService;

		public SearchController()
	    {
			_catalogueService = new RavenCatalogueService(DocumentStore);    
	    }


        //
        // GET: /Search/

        public ActionResult Index(string query)
        {
	        var collection = _catalogueService.Search(query);
			return View(collection);
        }

    }
}
