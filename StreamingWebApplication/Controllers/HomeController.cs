﻿using System.Web.Mvc;
using System.Web.UI;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.CatalogueService.Raven;

namespace StreamingWebApplication.Controllers
{
	public class HomeController : RavenController
	{
		private readonly ICatalogueService _catalogueService;

		public HomeController()
		{
			_catalogueService = new RavenCatalogueService(DocumentStore);
		}

		[OutputCache(Duration = 60*60*12,Location = OutputCacheLocation.Server)]
		public ActionResult Index()
		{			
			return View(_catalogueService.GetAllCarouselCollection());
		}


		public ActionResult About()
		{
			ViewBag.Message = "Your app description page.";

			return View();
		}

		public ActionResult Temp()
		{
			return PartialView();
		}
	}
}
