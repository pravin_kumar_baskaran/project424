﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Raven.Client;
using StreamingWebApplication.CatalogueService.Raven;

namespace StreamingWebApplication.Controllers
{
	public abstract class RavenController : Controller
	{
		private const string Data = "../../data/youtube";

		public static IDocumentStore DocumentStore
		{
			get
			{
				return MvcApplication.RavenDocumentStore;				
			}
		}

	}
}
