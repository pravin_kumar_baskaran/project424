﻿using System.Web.Mvc;
using System.Web.UI;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.CatalogueService.Raven;

namespace StreamingWebApplication.Controllers
{
    public class CollectionsController : RavenController
    {
		private readonly ICatalogueService _catalogueService;

	    public CollectionsController()
	    {
			_catalogueService = new RavenCatalogueService(DocumentStore);    
	    }

        //
        // GET: /Collections/
		[OutputCache(Duration = 60 * 60 * 12, Location = OutputCacheLocation.Server, VaryByParam = "id")]
        public ActionResult Index(string id)
        {
	        var collection = _catalogueService.GetCollection(id, "");
            return View(collection);
        }

    }
}
