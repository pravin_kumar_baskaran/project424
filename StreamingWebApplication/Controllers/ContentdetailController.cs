﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.CatalogueService.Raven;

namespace StreamingWebApplication.Controllers
{
    public class ContentdetailController : RavenController
    {
		private readonly ICatalogueService _catalogueService;

		public ContentdetailController()
		{
			_catalogueService = new RavenCatalogueService(DocumentStore);
		}

        //
        // GET: /Content/
		[OutputCache(Duration = 60 * 60 * 12, Location = OutputCacheLocation.Server,VaryByParam = "id")]
        public ActionResult Index(string id)
        {
			return View(_catalogueService.GetContentDetail(id));
        }

    }
}
