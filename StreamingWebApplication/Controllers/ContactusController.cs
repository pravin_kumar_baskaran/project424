﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using StreamingWebApplication.Models;

namespace StreamingWebApplication.Controllers
{
    public class ContactusController : Controller
    {
        //
        // GET: /Contactus/
		[HttpGet]
        public ActionResult Index()
        {
            return View();
        }

		[HttpPost]
		public ActionResult Index(ContactModel contactModel)
		{
			if (ModelState.IsValid)
			{
				try
				{
					var fromAddress = "info.kondatam@gmail.com";
					var fromPasssord = "pravinbaskaran";
					var toAddress = "info@kondatam.com";

					using (var smtp = new SmtpClient("smtp.gmail.com", 465))
					{
						smtp.EnableSsl = true;
						smtp.Timeout = 10000;
						var from = new MailAddress(fromAddress, contactModel.Name);
						var to = new MailAddress(toAddress, "Kondatam");
						var message = new MailMessage(from, to);
						
						var myCreds = new NetworkCredential(fromAddress, fromPasssord);
						smtp.UseDefaultCredentials = false;
						smtp.Credentials = myCreds;

						var sb = new StringBuilder();
						sb.Append("First name: " + contactModel.Name);
						sb.Append(Environment.NewLine);
						sb.Append("Email: " + contactModel.Email);
						sb.Append(Environment.NewLine);
						sb.Append("Message: " + contactModel.Message);
						message.Subject = "Contact Us";
						message.Body = sb.ToString();
						smtp.Send(message);
					}
					return View("Success");
				}
				catch (Exception)
				{
					return View("Error");
				}
			}
			return View();
		}

    }
}
