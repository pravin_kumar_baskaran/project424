﻿using System.ComponentModel.DataAnnotations;

namespace StreamingWebApplication.Models
{
	public class ContactModel
	{
		[Required]
		public string Name { get; set; }

		[Required]
		public string Email { get; set; }

		[Required]
		public string Message { get; set; }

	}
}