﻿using System.Collections;
using System.Collections.Generic;

namespace StreamingWebApplication.Extensions
{
	public static class ListExtensions
	{
		public static string Stringify(this IList<string> list )
		{
			return string.Join(",", list);
		}
	}
}