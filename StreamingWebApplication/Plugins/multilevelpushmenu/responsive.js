$(document).ready(function(){
	// HTML markup implementation, overlap mode
	$( '#menu' ).multilevelpushmenu({
		containersToPush: [],
		menuWidth: '25%',
		menuHeight: '100%',
		collapsed: true,
		swipe: 'both',
		direction:'rtl'
	});
});

$(window).resize(function () {
	$( '#menu' ).multilevelpushmenu( 'redraw' );
});
