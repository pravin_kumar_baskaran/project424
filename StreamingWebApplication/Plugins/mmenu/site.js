﻿$(document).ready(function () {	
	
	$("#search").mmenu({
		autoHeight: true,
		"offCanvas": {
			"position": "top",
			"zposition": "front"
		},
		dragOpen: {
			// drag open options
		}

	});

	$("#menu").mmenu({
		autoHeight: true,
		"offCanvas": {
			"position": "top",
			"zposition": "front"
		},
		dragOpen: {
			// drag open options
		}
	});

	var menuAPI = $("#menu").data("mmenu");
	var searchAPI = $("#search").data("mmenu");

	$(".closemenu").click(function () {
		menuAPI.close();
		searchAPI.close();
	});

});