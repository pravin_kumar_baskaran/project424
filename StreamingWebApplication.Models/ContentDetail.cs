﻿using System;
using System.Collections.Generic;

namespace StreamingWebApplication.Models
{
	public class ContentDetail
	{
		public string Id { get; set; }

		public string Link { get; set; }

		public string Title { get; set; }

		public DateTime ReleasedOn { get; set; }

		public string Synopsis { get; set; }
		
		public IList<string> Keywords { get; set; }

		public IList<string> Directors { get; set; }

		public IList<string> Actors { get; set; }

		public double LengthSecs
		{
			get
			{
				try
				{
					if (string.IsNullOrWhiteSpace(DurationIsoFormat))
						return double.MaxValue;

					return System.Xml.XmlConvert.ToTimeSpan(DurationIsoFormat).TotalSeconds;
				}
				catch (Exception)
				{
					return 0;
				}
			}
		}


		public IList<string> ThumbnailImagePath { get; set; }

		public DateTime CreatedOn { get; set; }
		
		public DateTime ModifiedOn { get; set; }

		public string DurationIsoFormat { get; set; }

		public ulong ViewsCount { get; set; }


		public string SelfHostedLink { get; set; }

	}
}