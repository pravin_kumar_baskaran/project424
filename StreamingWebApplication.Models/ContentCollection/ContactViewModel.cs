﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StreamingWebApplication.Models.ContentCollection
{
	public class ContentViewModel
	{
		public string Title { get; set; }

		public string ThumbnailImagePath { get; set; }

		public string ContentId { get; set; }

		public ulong ViewsCount { get; set; }

		public double Popularity
		{
			get
			{
				var val = ViewsCount;
				if (val > 10000)
					val = 10000;
				return Math.Round((double)val * 100 / 10000, 0);
			}
		}
		public string DurationIsoFormat { get; set; }


		public string DurationMinSec
		{
			get
			{
				try
				{
					if (string.IsNullOrWhiteSpace(DurationIsoFormat))
						return string.Empty;

					var timeSpan = System.Xml.XmlConvert.ToTimeSpan(DurationIsoFormat);
					return timeSpan.Minutes.ToString("D2") + ":" + timeSpan.Seconds.ToString("D2");
				}
				catch (Exception)
				{
					return string.Empty;
				}
			}
		}

		public ContentViewModel()
		{

		}

		public string VideoLink { get; set; }

		public ContentViewModel(ContentDetail contentDetail)
		{
			ContentId = contentDetail.Link;
			ThumbnailImagePath = contentDetail.ThumbnailImagePath.FirstOrDefault();
			Title = contentDetail.Title;
			DurationIsoFormat = contentDetail.DurationIsoFormat;
			ViewsCount = contentDetail.ViewsCount;
			VideoLink = string.IsNullOrWhiteSpace(contentDetail.SelfHostedLink)
				? contentDetail.Link
				: contentDetail.SelfHostedLink;
		}
	}

	public class ContentDetalViewModel : ContentViewModel
	{
		public string KeyWords { get; set; }

		public string Year { get; set; }

		public ContentDetalViewModel()
		{

		}

		public ContentDetalViewModel(ContentDetail contentDetail)
			: base(contentDetail)
		{

			KeyWords = string.Join(",", contentDetail.Keywords);
			Year = contentDetail.ReleasedOn.Year.ToString();

		}
	}
}