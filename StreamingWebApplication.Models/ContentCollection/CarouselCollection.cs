﻿using System.Collections.Generic;

namespace StreamingWebApplication.Models.ContentCollection
{
	public class CarouselCollection
	{
		public List<Carousel> Carousels { get; set; }

		public CarouselCollection()
		{
			Carousels = new List<Carousel>();
		}
	}
}