﻿using System.Collections;
using System.Collections.Generic;

namespace StreamingWebApplication.Models.ContentCollection
{
	public class Collection
	{
		public string Id { get; set; }
		
		public string Title { get; set; }

		public IList<ContentViewModel> ContentThumbnails { get; set; }

		public Collection()
		{
			ContentThumbnails = new List<ContentViewModel>();
		}
 	}
}
