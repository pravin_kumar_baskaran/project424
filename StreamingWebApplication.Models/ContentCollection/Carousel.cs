﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace StreamingWebApplication.Models.ContentCollection
{
	public class Carousel
	{
		public string Title { get; set; }

		public HashSet<string> Keywords { get; set; }
		public IList<ContentViewModel> CarouselItems { get; set; }



		public double DurationConstraintSecs { get; set; }

		public bool OrderbyLatest { get; set; }

		public Carousel()
		{
			CarouselItems= new List<ContentViewModel>();
			Keywords = new HashSet<string>();
			DurationConstraintSecs = double.MaxValue;
			OrderbyLatest = false;
		}
	}

}
