﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using StreamingWebApplication.Models;
using StreamingWebApplication.Models.ContentCollection;

namespace StreamingWebApplication.CatalogueService.Interface
{
	public interface ICatalogueService
	{
		CarouselCollection GetAllCarouselCollection();

		ContentDetalViewModel GetContentDetail(string id);

		ContentDetail Save(ContentDetail contentDetail);

		IList<ContentDetail> GetContentDetails();

		Collection GetCollection(string id, string sort);

		Collection Search(string query);



	}
}
