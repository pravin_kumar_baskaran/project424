﻿using System.Collections.Generic;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.Models;
using StreamingWebApplication.Models.ContentCollection;

namespace StreamingWebApplication.CatalogueService
{
	public class MockCatalogueService :ICatalogueService
	{
		private static ContentViewModel _caraouselItem1 = new ContentViewModel() {ContentId = "https://www.youtube.com/embed/rk7NYXql2dc?rel=0&amp;controls=0&amp;showinfo=0"};
		public CarouselCollection GetAllCarouselCollection()
		{
			return new CarouselCollection
			{
				Carousels = new List<Carousel>()
				{
					new Carousel()
					{
						Title = "Action Thriller",
						CarouselItems = new List<ContentViewModel>(){ _caraouselItem1,_caraouselItem1,_caraouselItem1, _caraouselItem1,_caraouselItem1,_caraouselItem1}
					},
					new Carousel()
					{
						Title = "Romantic Comedy",
						CarouselItems = new List<ContentViewModel>(){ _caraouselItem1,_caraouselItem1,_caraouselItem1, _caraouselItem1,_caraouselItem1,_caraouselItem1}
					},
					new Carousel()
					{
						Title = "Ghost Comedy",
						CarouselItems = new List<ContentViewModel>(){ _caraouselItem1,_caraouselItem1,_caraouselItem1, _caraouselItem1,_caraouselItem1,_caraouselItem1}
					}
				}
			};
		}

		public ContentDetalViewModel GetContentDetail(string id)
		{
			return new ContentDetalViewModel()
			{
				VideoLink = "https://www.youtube.com/embed/UhRI-IGHmb0?list=PLiJk2KIk0Obr_J2h2ykL7iTsfnv0N9gLQ&amp;showinfo=0",
				//Synopsis = "pppppppppppppppppppppppppppppppppppppppppp",
				Title = "qwerty"
			};
		}

		public ContentDetail Save(ContentDetail contentDetail)
		{
			throw new System.NotImplementedException();
		}

		public IList<ContentDetail> GetContentDetails()
		{
			throw new System.NotImplementedException();
		}

		public Collection GetCollection(string id, string sort)
		{
			throw new System.NotImplementedException();
		}

		public Collection Search(string query)
		{
			throw new System.NotImplementedException();
		}
	}
}
