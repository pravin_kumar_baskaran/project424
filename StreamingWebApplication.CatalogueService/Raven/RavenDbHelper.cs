﻿using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Embedded;

namespace StreamingWebApplication.CatalogueService.Raven
{
	public  class RavenDbHelper
	{
		static IDocumentStore _documentStore;

		private RavenDbHelper()
		{
			
		}

		public static IDocumentStore GetDocumentStore()
		{

			if (_documentStore == null)
			{
				_documentStore = new DocumentStore()
				{
					Url = "http://www.kondatam.com/dbservice",
					DefaultDatabase = "kondatam"
				};
				_documentStore.Conventions.IdentityPartsSeparator = "-";
				_documentStore.Initialize();
			}
			return _documentStore;
		}
	}
}
