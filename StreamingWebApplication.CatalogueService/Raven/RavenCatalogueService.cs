﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SqlServer.Server;
using Raven.Client;
using StreamingWebApplication.CatalogueService.Interface;
using StreamingWebApplication.Models;
using StreamingWebApplication.Models.ContentCollection;

namespace StreamingWebApplication.CatalogueService.Raven
{
	public class RavenCatalogueService : ICatalogueService
	{
		private readonly IDocumentStore _documentStore;

		public RavenCatalogueService(IDocumentStore documentStore)
		{
			_documentStore = documentStore;
		}

		public CarouselCollection GetAllCarouselCollection()
		{
			var carouselCollection = new CarouselCollection
			{
				Carousels = new List<Carousel>()
				{
					new Carousel() {Title = "Puthusu", OrderbyLatest = true},
					new Carousel() {Title = "Quickie", DurationConstraintSecs = 5*60,Keywords = _genre},
					new Carousel() {Title = "Thrilling Horror", Keywords = new HashSet<string>() {"Thriller", "Horror"}},
					new Carousel() {Title = "Comedy Drama", Keywords = new HashSet<string>() {"Comedy","Drama"}},
					new Carousel() {Title = "Love Story", Keywords = new HashSet<string>() {"Romance"}},
					new Carousel() {Title = "Social Awareness", Keywords = new HashSet<string>() {"Awareness"}}
				}
			};

			
			using (var session = _documentStore.OpenSession())
			{
				var contentDetails = session.Query<ContentDetail>().Take(1000).ToList();
				carouselCollection.Carousels.ForEach(c =>
				{
					if (c.OrderbyLatest)
					{
						c.CarouselItems = contentDetails							
							.OrderByDescending(cd => cd.CreatedOn)
							.Select(
								cd2 =>
									new ContentViewModel(cd2))
							.Take(15)
							.ToList();

					}
					else
					{
						c.CarouselItems = contentDetails
							.Where(cd => cd.Keywords.Intersect(c.Keywords, StringComparer.InvariantCultureIgnoreCase).Any())
							.Where(cd => cd.LengthSecs < c.DurationConstraintSecs)
							.OrderByDescending(cd => cd.ViewsCount)
							.Select(
								cd2 =>
									new ContentViewModel(cd2))
							.Take(15)
							.ToList();

					}

				});
			}

			return carouselCollection;
		}

		public ContentDetalViewModel GetContentDetail(string id)
		{
			using (IDocumentSession session = _documentStore.OpenSession())
			{
				var contentDetail = session.Query<ContentDetail>().FirstOrDefault(c => c.Link == id);

				if (contentDetail == null)
					return null;

				return new ContentDetalViewModel(contentDetail);

			}
		}

		public ContentDetail Save(ContentDetail contentDetail)
		{
			return contentDetail;

		}

		public IList<ContentDetail> GetContentDetails()
		{
			throw new System.NotImplementedException();
		}

		public Collection GetCollection(string id, string sort)
		{
			var returnList = new Collection();

			// is it genre
			if (_genre.Contains(id.ToLower()))
			{
				returnList.Id = id;
				returnList.Title = id;

				using (var session = _documentStore.OpenSession())
				{
					returnList.ContentThumbnails = session.Query<ContentDetail>().Take(1000).ToList()
						.Where(cd => cd.Keywords.Contains(id))
						.OrderByDescending(cd => cd.ViewsCount)
						.Select(
							cd2 =>
								new ContentViewModel(cd2))
						.ToList();
				}
			}

			return returnList;
		}

		public Collection Search(string query)
		{
			var returnList = new Collection();

			using (var session = _documentStore.OpenSession())
			{
				returnList.ContentThumbnails = session.Query<ContentDetail>().Take(1000).ToList()
					.Where(
						cd => cd.Keywords.Contains(query) || 
						cd.Title.IndexOf(query,StringComparison.InvariantCultureIgnoreCase)>=0)
					.Select(
						cd2 =>
							new ContentViewModel()
							{
								ContentId = cd2.Link,
								ThumbnailImagePath = cd2.ThumbnailImagePath.FirstOrDefault(),
								Title = cd2.Title
							})
					.ToList();
			}

			return returnList;
		}

		private readonly HashSet<string>  _genre= new HashSet<string>()
		{"comedy","horror","awareness","romance","thriller","action","drama"};
	}
}
